# FTP 
Socket programming is a way of connecting two nodes on a network to communicate with each other. One socket(node) listens on a particular port at an IP, while the other socket reaches out to the other to form a connection. The server forms the listener socket while the client reaches out to the server.

<img src="https://files.realpython.com/media/sockets-tcp-flow.1da426797e37.jpg" width="50%">

# Task
Now you should create server and client class to send files (image and pdf) from server to client and save that .remember that you have to use object-oriented programming and also we expect your code to be clean(see this [link](https://gist.github.com/wojteklu/73c6914cc446146b8b533c0988cf8d29)).

Note:      
1. Do not code your main solution in the main class.

2. You are not allowed to use readAllBytes function.

3. File should send not copy.

Bonus :

You can send your file using multi_thread then open more than one socket and send the file simultaneously. (for more information see this [link](https://medium.com/tech-and-the-city/multithreaded-server-23ad01aa2b98).)
